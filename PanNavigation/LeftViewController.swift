//
//  LeftViewController.swift
//  Chartz
//
//  Created by k ely on 1/12/15.
//  Copyright (c) 2015 flyingfresh. All rights reserved.
//

import UIKit

protocol LeftViewControllerDelegate {
//  e.g. func chartSelected(parameters: Parameters)
}

class LeftViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  var delegate: LeftViewControllerDelegate?
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}

// MARK: UITableViewDataSource

extension LeftViewController: UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
    return cell
  }
}

// Mark: Table View Delegate

extension LeftViewController: UITableViewDelegate {
  func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
//    delegate?.chartSelected(charts[indexPath.row])
  }
}
