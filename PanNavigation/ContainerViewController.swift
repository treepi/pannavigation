//
//  ContainerViewController.swift
//  Chartz
//
//  Created by k ely on 1/12/15.
//  Copyright (c) 2015 flyingfresh. All rights reserved.
//

import UIKit
import QuartzCore

enum toggleState {
  case expanded
  case collapsed
}

class ContainerViewController: UIViewController {
  var centerNavigationController: UINavigationController!
  var centerViewController: CenterViewController!
  var leftViewController: LeftViewController?
  let expandedOffset: CGFloat = 70
  var currentState: toggleState = .collapsed
  
  override func viewDidLoad() {
    super.viewDidLoad()
    centerViewController = UIStoryboard.centerViewController()
    centerViewController.delegate = self
    centerNavigationController = UINavigationController(rootViewController: centerViewController)
    view.addSubview(centerNavigationController.view)
    addChildViewController(centerNavigationController)
    centerNavigationController.didMoveToParentViewController(self)
    let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: "handlePanGesture:")
    centerNavigationController.view.addGestureRecognizer(panGestureRecognizer)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  // MARK: Pan Navigation
  
  func handlePanGesture(recognizer: UIPanGestureRecognizer) {
    let gestureIsDraggingFromLeftToRight = (recognizer.velocityInView(view).x > 0)
    switch(recognizer.state) {
    case .Began:
      if (currentState == .collapsed) {
        if (gestureIsDraggingFromLeftToRight) {
          addLeftViewController()
        }
      }
      centerNavigationController.view.layer.shadowOpacity = 0.7
    case .Changed:
      if gestureIsDraggingFromLeftToRight == false && currentState == .collapsed {
        // do nothing ;)
      } else {
        recognizer.view!.center.x = recognizer.view!.center.x + recognizer.translationInView(view).x
        recognizer.setTranslation(CGPointZero, inView: view)
      }

    case .Ended:
      if (leftViewController != nil) {
        let hasMovedGreaterThanHalfway = recognizer.view!.center.x > view.bounds.size.width
        animateLeftViewController(shouldExpand: hasMovedGreaterThanHalfway)
      }
    default:
      break
    }
  }
  
  func addLeftViewController() {
    leftViewController = UIStoryboard.leftViewController()
    leftViewController!.delegate = centerViewController
    view.insertSubview(leftViewController!.view, atIndex: 0)
    addChildViewController(leftViewController!)
    leftViewController!.didMoveToParentViewController(self)
  }
  
  func animateLeftViewController(#shouldExpand: Bool) {
    if (shouldExpand) {
      currentState = .expanded
      animatePositionX(destination: CGRectGetWidth(centerNavigationController.view.frame) - expandedOffset)
    } else {
      animatePositionX(destination: 0) { finished in
        self.currentState = .collapsed
        self.leftViewController!.view.removeFromSuperview()
        self.leftViewController = nil;
      }
    }
  }
  
  func animatePositionX(#destination: CGFloat, completion: ((Bool) -> ())! = nil) {
    UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: .CurveEaseInOut, animations: {
      self.centerNavigationController.view.frame.origin.x = destination
      }, completion: completion)
  }
}

extension ContainerViewController: CenterViewControllerDelegate {
  func toggle() {
    let shouldExpand = currentState == .collapsed
    if shouldExpand {
      addLeftViewController()
      centerNavigationController.view.layer.shadowOpacity = 0.7
    }
    else {
      centerNavigationController.view.layer.shadowOpacity = 0.0
    }
    animateLeftViewController(shouldExpand: shouldExpand)
  }
}

extension UIStoryboard {
  class func mainStoryboard() -> UIStoryboard {
    return UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
  }
  
  class func leftViewController() -> LeftViewController? {
    return mainStoryboard().instantiateViewControllerWithIdentifier("LeftViewController") as? LeftViewController
  }
  class func centerViewController() -> CenterViewController? {
    return mainStoryboard().instantiateViewControllerWithIdentifier("CenterViewController") as? CenterViewController
  }
}
