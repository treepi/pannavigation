//
//  CenterViewController.swift
//  Chartz
//
//  Created by k ely on 1/12/15.
//  Copyright (c) 2015 flyingfresh. All rights reserved.
//

import UIKit

@objc protocol CenterViewControllerDelegate {
  optional func toggle()
}

class CenterViewController: UIViewController {
  
  var delegate: CenterViewControllerDelegate?
  
  @IBAction func leftBarButtonItemTapped(sender: UIBarButtonItem) {
    if let d = delegate {
      d.toggle?()
    }
  }
  
  @IBAction func unwindToCenter(segue: UIStoryboardSegue) {
    println("Coming from BLUE")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}

extension CenterViewController: LeftViewControllerDelegate { }
